package fr.utc.flexin.responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Samy on 21/11/2017.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class CategoryResponse {
    protected int id;
    protected String name;
    protected Integer parent_id;

    public void setId(int id) { this.id = id; }
    public void setName(String name) { this.name = name; }
    public void setParentId(Integer id) { this.parent_id = id; }

    public int getId() { return this.id; }
    public String getName() { return this.name; }
    public Integer getParentId() { return this.parent_id; }
}
