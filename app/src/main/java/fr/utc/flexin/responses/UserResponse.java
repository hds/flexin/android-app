package fr.utc.flexin.responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserResponse {
    protected int id;
    protected String name;
    protected String lastname;
    protected String firstname;
    protected String email;
    protected String phone;
    protected String company;
    protected String type;

    public void setId(int id) { this.id = id; }
    public void setName(String name) { this.name = name; }
    public void setLastname(String name) { this.lastname = name; }
    public void setFirstname(String name) { this.firstname = name; }
    public void setEmail(String email) { this.email = email; }
    public void setPhone(String phone) { this.phone = phone; }
    public void setCompany(String company) { this.company = company; }
    public void setType(String type) { this.type = type; }

    public int getId() { return this.id; }
    public String getName() { return name; }
    public String getLastname() { return lastname; }
    public String getFirstname() { return firstname; }
    public String getEmail() { return email; }
    public String getPhone() { return phone; }
    public String getCompany() { return company; }
    public String getType() { return type; }
}
