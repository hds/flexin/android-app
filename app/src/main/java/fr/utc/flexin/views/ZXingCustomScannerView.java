package fr.utc.flexin.views;

import android.content.Context;
import android.util.AttributeSet;

import fr.utc.flexin.tools.ExtendedScannerView;
import me.dm7.barcodescanner.core.IViewFinder;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ZXingCustomScannerView extends ZXingScannerView {
    public ZXingCustomScannerView(Context context) {
        super(context);
    }

    public ZXingCustomScannerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override
    protected IViewFinder createViewFinderView(Context context) {
        return new ExtendedScannerView(context);
    }
}
