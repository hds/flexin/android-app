package fr.utc.flexin;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

import fr.utc.flexin.tools.Config;

/**
 * Created by Samy on 24/10/2017.
 */

public class MainActivity extends BaseActivity {
    private static final String LOG_TAG = "_MainActivity";

    private boolean casConnexionDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
/*
        findViewById(R.id.button_username).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAPIActivity(MainActivity.this);
            }
        });*/

        launch();
    }

    @Override
    public void onRestart() {
        super.onRestart();

        disconnect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        disconnect();
    }

    @Override
    protected void onIdentification(final String badgeId) {
        if (!dialog.isShowing()) {
            dialog.startLoading(MainActivity.this, getString(R.string.badge_dialog), getString(R.string.badge_recognization));

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        flexinAPI.loginBadge(badgeId);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                dialog.stopLoading();

                                startAPIActivity(MainActivity.this);
                            }
                        });
                    } catch (final Exception e) {
                        Log.e(LOG_TAG, "error: " + e.getMessage());

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (flexinAPI.getRequest().getResponseCode() == 404)
                                    dialog.errorDialog(MainActivity.this, getString(R.string.badge_dialog), getString(R.string.badge_error_not_recognized));
                                else
                                    dialog.errorDialog(MainActivity.this, getString(R.string.badge_dialog), e.getMessage());
                            }
                        });
                    }
                }
            }).start();
        }
    }

    @Override
    protected void enableInternetDialog(final Context context) {
        Toast.makeText(context, R.string.internet_not_available, Toast.LENGTH_SHORT).show();

        dialog.infoDialog(MainActivity.this, getString(R.string.connection), getString(R.string.internet_accessibility), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (!checkInternet(context))
                    enableInternetDialog(context);
                else
                    restartApp(MainActivity.this);
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5 && keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            onBackPressed();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        Intent setIntent = new Intent(Intent.ACTION_MAIN);
        setIntent.addCategory(Intent.CATEGORY_HOME);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(setIntent);
    }

    @Override
    protected void unregister(final Activity activity) {
        super.unregister(activity);
    }

    protected void launch() {
        sharedPreferences = getSharedPreferences("flexin", Activity.MODE_PRIVATE);

        config = new Config(sharedPreferences);
    }
}
