package fr.utc.flexin;

import android.os.Bundle;

import fr.utc.flexin.adapters.OptionsAdapter;

/**
 * Created by Samy on 18/11/2017.
 */

public class MaterialActivity extends BaseActivity {
    private static final String LOG_TAG = "_MaterialActivity";

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.material_activity);
    }
}

