package fr.utc.flexin;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.zxing.Result;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.utc.flexin.adapters.OptionsAdapter;
import fr.utc.flexin.responses.CategoryResponse;
import fr.utc.flexin.responses.InfrastructureResponse;
import fr.utc.flexin.responses.UserResponse;
import fr.utc.flexin.views.ZXingCustomScannerView;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static android.hardware.Camera.CameraInfo.CAMERA_FACING_BACK;

/**
 * Created by Samy on 18/11/2017.
 */

public class ReaderActivity extends BaseActivity implements ZXingScannerView.ResultHandler {
    private static final String LOG_TAG = "_ReaderActivity";

    protected TextView scannerText;
    protected ZXingCustomScannerView scannerView;
    protected Button scannerButton;

    protected Runnable buttonRunnable;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reader_activity);

        this.scannerText = findViewById(R.id.scanner_text);
        this.scannerView = findViewById(R.id.scanner);
        this.scannerButton = findViewById(R.id.scanner_button);

        this.scannerView.setResultHandler(ReaderActivity.this);
        this.scannerView.startCamera(CAMERA_FACING_BACK);

        this.scannerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (buttonRunnable == null) {
                    try {
                        openMenu();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else {
                    Runnable toRun = buttonRunnable;
                    buttonRunnable = null;

                    toRun.run();
                }
            }
        });

        defDialog();
    }

    @Override
    public void onStart() {
        super.onStart();
        
        defDialog();

        this.scannerView.setResultHandler(this);
        this.scannerView.startCamera();
    }

    @Override
    public void onResume() {
        super.onResume();

        defDialog();

        if (!dialog.isShowing())
            resumeReading();
    }

    @Override
    public void onStop() {
        super.onStop();

        this.scannerView.stopCameraPreview();
        this.scannerView.stopCamera();
    }

    @Override
    public void onIdentification(final String badgeId) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ReaderActivity.this);
        alertDialogBuilder
            .setTitle(R.string.nfc_reading)
            .setMessage(R.string.nfc_not_recognized)
            .setCancelable(false)
            .setNegativeButton(R.string.ok,null)
            .setNeutralButton(R.string.add, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    createNewMaterial("badgeId", badgeId);
                }
            }
        );

        dialog.createDialog(alertDialogBuilder);
    }

    @Override
    protected void enableInternetDialog(final Context context) {
        Toast.makeText(context, R.string.internet_not_available, Toast.LENGTH_SHORT).show();

        dialog.infoDialog(ReaderActivity.this, getString(R.string.connection), getString(R.string.internet_accessibility), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (!checkInternet(context))
                    enableInternetDialog(context);
                else
                    restartApp(ReaderActivity.this);
            }
        });
    }

    protected void defDialog() {
        this.dialog.attachOnOpen(new Runnable() {
            @Override
            public void run() {
                pauseReading();
            }
        });
        this.dialog.attachOnClose(new Runnable() {
            @Override
            public void run() {
                resumeReading();
            }
        });
    }

    protected void openMenu() {
        List<String> options = Arrays.asList(getResources().getStringArray(R.array.options));

        dialog.createMenu(
            ReaderActivity.this,
            "Menu",
            options,
            new Runnable() {
                @Override
                public void run() {
                    switch (dialog.getOptionSelected()) {
                        case 0:
                            //openMaterialMenu();
                            break;
                        case 1:
                            openCategoryMenu();
                            break;
                        case 2:
                            openInfrastructureMenu();
                            break;
                        case 3:
                            openUserMenu();
                            break;
                    }
                }
            }
        );
    }

    protected void openCategoryMenu() {
        List<String> options = Arrays.asList(getResources().getStringArray(R.array.categoryOptions));

        dialog.createMenu(
            ReaderActivity.this,
            "Catégories",
            options,
            new Runnable() {
                @Override
                public void run() {
                    switch (dialog.getOptionSelected()) {
                        case 0:
                            listCategories();
                            break;
                        case 1:
                            addCategory();
                            break;
                        case 2:
                            openMenu();
                            break;
                    }
                }
            }
        );
    }

    protected void addCategory() {
        dialog.startLoading(ReaderActivity.this, getString(R.string.loading), getString(R.string.data_collection));

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    flexinAPI.getCategoryList();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dialog.stopLoading();

                            try {
                                final List<CategoryResponse> categories = new ObjectMapper().readValue(flexinAPI.getRequest().getResponse(), new TypeReference<List<CategoryResponse>>() {});
                                List<String> options = new ArrayList<String>() {{
                                    add("-- Sans catégorie parent --");
                                }};

                                for (CategoryResponse category : categories)
                                    options.add(category.getName());

                                final View menuView = getLayoutInflater().inflate(R.layout.fragment_category, null);
                                final TextView categoryText = menuView.findViewById(R.id.name_text);
                                final Spinner categorySpinner = menuView.findViewById(R.id.category_spinner);

                                ArrayNode optionListAdded = (ArrayNode) new ObjectMapper().valueToTree(options);
                                final Integer[] parentId = {null};
                                try {
                                    OptionsAdapter optionsAdapter = new OptionsAdapter(ReaderActivity.this, optionListAdded);
                                    categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()  {
                                        @Override
                                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                            if (position == 0)
                                                parentId[0] = null;
                                            else
                                                parentId[0] = categories.get(position - 1).getId();
                                        }

                                        @Override
                                        public void onNothingSelected(AdapterView<?> parent) {
                                            parentId[0] = null;
                                        }
                                    });
                                    categorySpinner.setAdapter(optionsAdapter);

                                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ReaderActivity.this);
                                    alertDialogBuilder
                                            .setTitle("Ajouter une nouvelle catégorie")
                                            .setView(menuView)
                                            .setCancelable(true)
                                            .setPositiveButton("Ajouter", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialoin, int which) {
                                                    dialog.startLoading(ReaderActivity.this, getString(R.string.loading), getString(R.string.data_collection));

                                                    new Thread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            try {
                                                                flexinAPI.addCategory(categoryText.getText().toString(), parentId[0]);
                                                                runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        dialog.stopLoading();

                                                                        try {
                                                                            dialog.infoDialog(ReaderActivity.this, "Catégorie", "Ajoutée avec succès", new DialogInterface.OnClickListener() {
                                                                                @Override
                                                                                public void onClick(DialogInterface dialog, int which) {
                                                                                    openCategoryMenu();
                                                                                }
                                                                            });
                                                                            Log.d(LOG_TAG, flexinAPI.getRequest().getResponse());
                                                                        } catch (Exception e) {
                                                                            e.printStackTrace();
                                                                        }
                                                                    }
                                                                });
                                                            } catch (final Exception e) {
                                                                Log.e(LOG_TAG, "error: " + e.getMessage());

                                                                runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        dialog.errorDialog(ReaderActivity.this, getString(R.string.data_collection), e.getMessage());
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    }).start();
                                                }
                                            });

                                    dialog.createDialog(alertDialogBuilder);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (final Exception e) {
                    Log.e(LOG_TAG, "error: " + e.getMessage());

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dialog.errorDialog(ReaderActivity.this, getString(R.string.data_collection), e.getMessage());
                        }
                    });
                }
            }
        }).start();
    }

    protected void listCategories() {
        dialog.startLoading(ReaderActivity.this, getString(R.string.loading), getString(R.string.data_collection));

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    flexinAPI.getCategoryList();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dialog.stopLoading();

                            try {
                                Log.d(LOG_TAG, flexinAPI.getRequest().getResponse());
                                final List<CategoryResponse> categories = new ObjectMapper().readValue(flexinAPI.getRequest().getResponse(), new TypeReference<List<CategoryResponse>>(){});

                                if (categories.size() == 0) {
                                    dialog.infoDialog(ReaderActivity.this, "Infrastructure", "Il n'y aucune catégorie de créée", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            openCategoryMenu();
                                        }
                                    });
                                }
                                else {
                                    List<String> options = new ArrayList<>();

                                    for (CategoryResponse category : categories)
                                        options.add(category.getName());

                                    dialog.createMenu(
                                            ReaderActivity.this,
                                            "Catégories",
                                            options,
                                            new Runnable() {
                                                @Override
                                                public void run() {
                                                    seeCategorie(categories.get(dialog.getOptionSelected()).getId());
                                                }
                                            }
                                    );
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (final Exception e) {
                    Log.e(LOG_TAG, "error: " + e.getMessage());

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dialog.errorDialog(ReaderActivity.this, getString(R.string.data_collection), e.getMessage());
                        }
                    });
                }
            }
        }).start();
    }

    protected void seeCategorie(final Integer id) {
        dialog.startLoading(ReaderActivity.this, getString(R.string.loading), getString(R.string.data_collection));

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    flexinAPI.getCategory(id);
                    final CategoryResponse categoryResponse = new ObjectMapper().readValue(flexinAPI.getRequest().getResponse(), CategoryResponse.class);

                    flexinAPI.getCategoryList();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dialog.stopLoading();

                            try {
                                final List<CategoryResponse> categories = new ObjectMapper().readValue(flexinAPI.getRequest().getResponse(), new TypeReference<List<CategoryResponse>>() {});
                                List<String> options = new ArrayList<String>() {{
                                    add("-- Sans catégorie parent --");
                                }};

                                for (CategoryResponse category : categories)
                                    options.add(category.getName());

                                final View menuView = getLayoutInflater().inflate(R.layout.fragment_category, null);
                                final TextView categoryText = menuView.findViewById(R.id.name_text);
                                final Spinner categorySpinner = menuView.findViewById(R.id.category_spinner);

                                ArrayNode optionListAdded = (ArrayNode) new ObjectMapper().valueToTree(options);
                                final Integer[] parentId = {null};
                                try {
                                    OptionsAdapter optionsAdapter = new OptionsAdapter(ReaderActivity.this, optionListAdded);
                                    categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()  {
                                        @Override
                                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                            if (position == 0)
                                                parentId[0] = null;
                                            else
                                                parentId[0] = categories.get(position - 1).getId();
                                        }

                                        @Override
                                        public void onNothingSelected(AdapterView<?> parent) {
                                            parentId[0] = null;
                                        }
                                    });
                                    categorySpinner.setAdapter(optionsAdapter);

                                    categoryText.setText(categoryResponse.getName());

                                    if (categoryResponse.getParentId() != null)
                                        categorySpinner.setSelection(categoryResponse.getParentId());

                                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ReaderActivity.this);
                                    alertDialogBuilder
                                            .setTitle("Catégorie")
                                            .setView(menuView)
                                            .setCancelable(true)
                                            .setPositiveButton("Modifier", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialoin, int which) {
                                                    dialog.startLoading(ReaderActivity.this, getString(R.string.loading), getString(R.string.data_collection));

                                                    new Thread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            try {
                                                                flexinAPI.editCategory(id, categoryText.getText().toString(), parentId[0]);
                                                                runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        dialog.stopLoading();

                                                                        try {
                                                                            dialog.infoDialog(ReaderActivity.this, "Catégorie", "Modifiée avec succès", new DialogInterface.OnClickListener() {
                                                                                @Override
                                                                                public void onClick(DialogInterface dialog, int which) {
                                                                                    openCategoryMenu();
                                                                                }
                                                                            });
                                                                            Log.d(LOG_TAG, flexinAPI.getRequest().getResponse());
                                                                        } catch (Exception e) {
                                                                            e.printStackTrace();
                                                                        }
                                                                    }
                                                                });
                                                            } catch (final Exception e) {
                                                                Log.e(LOG_TAG, "error: " + e.getMessage());

                                                                runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        dialog.errorDialog(ReaderActivity.this, getString(R.string.data_collection), e.getMessage());
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    }).start();
                                                }
                                            })
                                            .setNeutralButton("Supprimer", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogn, int which) {
                                                    dialog.startLoading(ReaderActivity.this, getString(R.string.loading), "Suppression de la catégorie");

                                                    new Thread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            try {
                                                                flexinAPI.delCategory(id);

                                                                runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        dialog.stopLoading();

                                                                        try {
                                                                            dialog.infoDialog(ReaderActivity.this, "Catégorie", "Supprimée avec succès", new DialogInterface.OnClickListener() {
                                                                                @Override
                                                                                public void onClick(DialogInterface dialog, int which) {
                                                                                    openCategoryMenu();
                                                                                }
                                                                            });
                                                                            Log.d(LOG_TAG, flexinAPI.getRequest().getResponse());
                                                                        } catch (Exception e) {
                                                                            dialog.infoDialog(ReaderActivity.this, "Catégorie", "Impossible de supprimer la catégorie", new DialogInterface.OnClickListener() {
                                                                                @Override
                                                                                public void onClick(DialogInterface dialog, int which) {
                                                                                    openCategoryMenu();
                                                                                }
                                                                            });
                                                                        }
                                                                    }
                                                                });
                                                            } catch (final Exception e) {
                                                                Log.e(LOG_TAG, "error: " + e.getMessage());

                                                                runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        dialog.errorDialog(ReaderActivity.this, getString(R.string.data_collection), e.getMessage());
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    }).run();
                                                }
                                            });

                                    dialog.createDialog(alertDialogBuilder);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (final Exception e) {
                    Log.e(LOG_TAG, "error: " + e.getMessage());

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dialog.errorDialog(ReaderActivity.this, getString(R.string.data_collection), e.getMessage());
                        }
                    });
                }
            }
        }).start();
    }

    protected void openInfrastructureMenu() {
        List<String> options = Arrays.asList(getResources().getStringArray(R.array.infrastructureOptions));

        dialog.createMenu(
                ReaderActivity.this,
                "Infrastructures",
                options,
                new Runnable() {
                    @Override
                    public void run() {
                        switch (dialog.getOptionSelected()) {
                            case 0:
                                listInfrastructures();
                                break;
                            case 1:
                                addInfrastructure();
                                break;
                            case 2:
                                openMenu();
                                break;
                        }
                    }
                }
        );
    }

    protected void addInfrastructure() {
        try {
            final View menuView = getLayoutInflater().inflate(R.layout.fragment_infrastructure, null);
            final TextView infrastructureText = menuView.findViewById(R.id.name_text);
            final TextView descriptionText = menuView.findViewById(R.id.description_text);

            try {
                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ReaderActivity.this);
                alertDialogBuilder
                        .setTitle("Ajouter une nouvelle infrastructure")
                        .setView(menuView)
                        .setCancelable(true)
                        .setPositiveButton("Ajouter", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialoin, int which) {
                                dialog.startLoading(ReaderActivity.this, getString(R.string.loading), getString(R.string.data_collection));

                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            flexinAPI.addInfrastructure(infrastructureText.getText().toString(), descriptionText.getText().toString());
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    dialog.stopLoading();

                                                    try {
                                                        dialog.infoDialog(ReaderActivity.this, "Infrastructure", "Ajoutée avec succès", new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which) {
                                                                openInfrastructureMenu();
                                                            }
                                                        });
                                                        Log.d(LOG_TAG, flexinAPI.getRequest().getResponse());
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            });
                                        } catch (final Exception e) {
                                            Log.e(LOG_TAG, "error: " + e.getMessage());

                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    dialog.errorDialog(ReaderActivity.this, getString(R.string.data_collection), e.getMessage());
                                                }
                                            });
                                        }
                                    }
                                }).start();
                            }
                        });

                dialog.createDialog(alertDialogBuilder);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void listInfrastructures() {
        dialog.startLoading(ReaderActivity.this, getString(R.string.loading), getString(R.string.data_collection));

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    flexinAPI.getInfrastructureList();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dialog.stopLoading();

                            try {
                                Log.d(LOG_TAG, flexinAPI.getRequest().getResponse());
                                final List<InfrastructureResponse> infrastructureResponseList = new ObjectMapper().readValue(flexinAPI.getRequest().getResponse(), new TypeReference<List<InfrastructureResponse>>(){});

                                if (infrastructureResponseList.size() == 0) {
                                    dialog.infoDialog(ReaderActivity.this, "Infrastructure", "Il n'y aucune infrastructure de créée", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            openInfrastructureMenu();
                                        }
                                    });
                                }
                                else {
                                    List<String> options = new ArrayList<>();

                                    for (InfrastructureResponse infrastructure : infrastructureResponseList)
                                        options.add(infrastructure.getName());

                                    dialog.createMenu(
                                            ReaderActivity.this,
                                            "Infrastructure",
                                            options,
                                            new Runnable() {
                                                @Override
                                                public void run() {
                                                    seeInfrastructure(infrastructureResponseList.get(dialog.getOptionSelected()).getId());
                                                }
                                            }
                                    );
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (final Exception e) {
                    Log.e(LOG_TAG, "error: " + e.getMessage());

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dialog.errorDialog(ReaderActivity.this, getString(R.string.data_collection), e.getMessage());
                        }
                    });
                }
            }
        }).start();
    }

    protected void seeInfrastructure(final Integer id) {
        dialog.startLoading(ReaderActivity.this, getString(R.string.loading), getString(R.string.data_collection));

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    flexinAPI.getInfrastructure(id);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dialog.stopLoading();

                            try {
                                final InfrastructureResponse infrastructureResponse = new ObjectMapper().readValue(flexinAPI.getRequest().getResponse(), InfrastructureResponse.class);

                                final View menuView = getLayoutInflater().inflate(R.layout.fragment_infrastructure, null);
                                final TextView infrastructureText = menuView.findViewById(R.id.name_text);
                                final TextView descriptionText = menuView.findViewById(R.id.description_text);

                                try {
                                    infrastructureText.setText(infrastructureResponse.getName());
                                    descriptionText.setText(infrastructureResponse.getDescription());

                                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ReaderActivity.this);
                                    alertDialogBuilder
                                            .setTitle("Infrastructure")
                                            .setView(menuView)
                                            .setCancelable(true)
                                            .setPositiveButton("Modifier", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialoin, int which) {
                                                    dialog.startLoading(ReaderActivity.this, getString(R.string.loading), getString(R.string.data_collection));

                                                    new Thread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            try {
                                                                flexinAPI.editInfrastructure(id, infrastructureText.getText().toString(), descriptionText.getText().toString());
                                                                runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        dialog.stopLoading();

                                                                        try {
                                                                            dialog.infoDialog(ReaderActivity.this, "Infrastructure", "Modifiée avec succès", new DialogInterface.OnClickListener() {
                                                                                @Override
                                                                                public void onClick(DialogInterface dialog, int which) {
                                                                                    openInfrastructureMenu();
                                                                                }
                                                                            });
                                                                            Log.d(LOG_TAG, flexinAPI.getRequest().getResponse());
                                                                        } catch (Exception e) {
                                                                            e.printStackTrace();
                                                                        }
                                                                    }
                                                                });
                                                            } catch (final Exception e) {
                                                                Log.e(LOG_TAG, "error: " + e.getMessage());

                                                                runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        dialog.errorDialog(ReaderActivity.this, getString(R.string.data_collection), e.getMessage());
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    }).start();
                                                }
                                            })
                                            .setNeutralButton("Supprimer", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogn, int which) {
                                                    dialog.startLoading(ReaderActivity.this, getString(R.string.loading), "Suppression de l'infrastructure");

                                                    new Thread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            try {
                                                                flexinAPI.delInfrastructure(id);

                                                                runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        dialog.stopLoading();

                                                                        try {
                                                                            dialog.infoDialog(ReaderActivity.this, "Infrastructure", "Supprimée avec succès", new DialogInterface.OnClickListener() {
                                                                                @Override
                                                                                public void onClick(DialogInterface dialog, int which) {
                                                                                    openInfrastructureMenu();
                                                                                }
                                                                            });
                                                                            Log.d(LOG_TAG, flexinAPI.getRequest().getResponse());
                                                                        } catch (Exception e) {
                                                                            dialog.infoDialog(ReaderActivity.this, "Infrastructure", "Impossible de supprimer l'infrastructure", new DialogInterface.OnClickListener() {
                                                                                @Override
                                                                                public void onClick(DialogInterface dialog, int which) {
                                                                                    openInfrastructureMenu();
                                                                                }
                                                                            });
                                                                        }
                                                                    }
                                                                });
                                                            } catch (final Exception e) {
                                                                Log.e(LOG_TAG, "error: " + e.getMessage());

                                                                runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        dialog.errorDialog(ReaderActivity.this, getString(R.string.data_collection), e.getMessage());
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    }).run();
                                                }
                                            });

                                    dialog.createDialog(alertDialogBuilder);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (final Exception e) {
                    Log.e(LOG_TAG, "error: " + e.getMessage());

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dialog.errorDialog(ReaderActivity.this, getString(R.string.data_collection), e.getMessage());
                        }
                    });
                }
            }
        }).start();
    }

    protected void openUserMenu() {
        List<String> options = Arrays.asList(getResources().getStringArray(R.array.userOptions));

        dialog.createMenu(
                ReaderActivity.this,
                "Utilisateurs",
                options,
                new Runnable() {
                    @Override
                    public void run() {
                        switch (dialog.getOptionSelected()) {
                            case 0:
                                listUsers();
                                break;
                            case 1:
                                addUser();
                                break;
                            case 2:
                                openMenu();
                                break;
                        }
                    }
                }
        );
    }

    protected void addUser() {
        try {
            final View menuView = getLayoutInflater().inflate(R.layout.fragment_user, null);
            final TextView lastnameText = menuView.findViewById(R.id.lastname_text);
            final TextView firstnameText = menuView.findViewById(R.id.firstname_text);
            final TextView emailText = menuView.findViewById(R.id.email_text);
            final TextView phoneText = menuView.findViewById(R.id.phone_text);
            final TextView companyText = menuView.findViewById(R.id.company_text);
            final Spinner typeSpinner = menuView.findViewById(R.id.type_spinner);

            final String[] type = {null};
            try {
                final List<String> options = Arrays.asList(getResources().getStringArray(R.array.userType));
                ArrayNode optionsNode = new ObjectMapper().createArrayNode();

                for (String option : options)
                    optionsNode.add(option);

                OptionsAdapter optionsAdapter = new OptionsAdapter(ReaderActivity.this, optionsNode);
                typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()  {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        type[0] = options.get(position);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        type[0] = options.get(0);
                    }
                });
                typeSpinner.setAdapter(optionsAdapter);

                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ReaderActivity.this);
                alertDialogBuilder
                        .setTitle("Ajouter un nouvel utilisateur")
                        .setView(menuView)
                        .setCancelable(true)
                        .setPositiveButton("Ajouter", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialoin, int which) {
                                dialog.startLoading(ReaderActivity.this, getString(R.string.loading), getString(R.string.data_collection));

                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            flexinAPI.addUser(lastnameText.getText().toString(), firstnameText.getText().toString(), emailText.getText().toString(), phoneText.getText().toString(), companyText.getText().toString(), type[0]);
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    dialog.stopLoading();

                                                    try {
                                                        dialog.infoDialog(ReaderActivity.this, "Utlisateur", "Ajouté avec succès", new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which) {
                                                                openUserMenu();
                                                            }
                                                        });
                                                        Log.d(LOG_TAG, flexinAPI.getRequest().getResponse());
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            });
                                        } catch (final Exception e) {
                                            Log.e(LOG_TAG, "error: " + e.getMessage());

                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    dialog.errorDialog(ReaderActivity.this, getString(R.string.data_collection), e.getMessage());
                                                }
                                            });
                                        }
                                    }
                                }).start();
                            }
                        });

                dialog.createDialog(alertDialogBuilder);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void listUsers() {
        dialog.startLoading(ReaderActivity.this, getString(R.string.loading), getString(R.string.data_collection));

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    flexinAPI.getUserList();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dialog.stopLoading();

                            try {
                                Log.d(LOG_TAG, flexinAPI.getRequest().getResponse());
                                final List<UserResponse> userResponseList = new ObjectMapper().readValue(flexinAPI.getRequest().getResponse(), new TypeReference<List<UserResponse>>(){});

                                if (userResponseList.size() == 0) {
                                    dialog.infoDialog(ReaderActivity.this, "Utlisateurs", "Il n'existe aucun utlisateur", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            openUserMenu();
                                        }
                                    });
                                }
                                else {
                                    List<String> options = new ArrayList<>();

                                    for (UserResponse user : userResponseList)
                                        options.add(user.getName());

                                    dialog.createMenu(
                                            ReaderActivity.this,
                                            "Utilisateurs",
                                            options,
                                            new Runnable() {
                                                @Override
                                                public void run() {
                                                    seeUser(userResponseList.get(dialog.getOptionSelected()).getId());
                                                }
                                            }
                                    );
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (final Exception e) {
                    Log.e(LOG_TAG, "error: " + e.getMessage());

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dialog.errorDialog(ReaderActivity.this, getString(R.string.data_collection), e.getMessage());
                        }
                    });
                }
            }
        }).start();
    }

    protected void seeUser(final Integer id) {
        dialog.startLoading(ReaderActivity.this, getString(R.string.loading), getString(R.string.data_collection));

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    flexinAPI.getUser(id);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dialog.stopLoading();

                            try {
                                final UserResponse userResponse = new ObjectMapper().readValue(flexinAPI.getRequest().getResponse(), UserResponse.class);

                                final View menuView = getLayoutInflater().inflate(R.layout.fragment_user, null);
                                final TextView lastnameText = menuView.findViewById(R.id.lastname_text);
                                final TextView firstnameText = menuView.findViewById(R.id.firstname_text);
                                final TextView emailText = menuView.findViewById(R.id.email_text);
                                final TextView phoneText = menuView.findViewById(R.id.phone_text);
                                final TextView companyText = menuView.findViewById(R.id.company_text);
                                final Spinner typeSpinner = menuView.findViewById(R.id.type_spinner);

                                final String[] type = {null};
                                try {
                                    final List<String> options = Arrays.asList(getResources().getStringArray(R.array.userType));
                                    ArrayNode optionsNode = new ObjectMapper().createArrayNode();

                                    for (String option : options)
                                        optionsNode.add(option);

                                    OptionsAdapter optionsAdapter = new OptionsAdapter(ReaderActivity.this, optionsNode);
                                    typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()  {
                                        @Override
                                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                            type[0] = options.get(position);
                                        }

                                        @Override
                                        public void onNothingSelected(AdapterView<?> parent) {
                                            type[0] = options.get(0);
                                        }
                                    });
                                    typeSpinner.setAdapter(optionsAdapter);

                                    firstnameText.setText(userResponse.getFirstname());
                                    lastnameText.setText(userResponse.getLastname());
                                    emailText.setText(userResponse.getEmail());
                                    phoneText.setText(userResponse.getPhone());
                                    companyText.setText(userResponse.getCompany());
                                    typeSpinner.setSelection(options.indexOf(userResponse.getType()));

                                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ReaderActivity.this);
                                    alertDialogBuilder
                                            .setTitle("Modifier l\'utilisateur")
                                            .setView(menuView)
                                            .setCancelable(true)
                                            .setPositiveButton("Modifier", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialoin, int which) {
                                                    dialog.startLoading(ReaderActivity.this, getString(R.string.loading), getString(R.string.data_collection));

                                                    new Thread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            try {
                                                                flexinAPI.editUser(lastnameText.getText().toString(), firstnameText.getText().toString(), emailText.getText().toString(), phoneText.getText().toString(), companyText.getText().toString(), type[0]);
                                                                runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        dialog.stopLoading();

                                                                        try {
                                                                            dialog.infoDialog(ReaderActivity.this, "Utlisateur", "Modifié avec succès", new DialogInterface.OnClickListener() {
                                                                                @Override
                                                                                public void onClick(DialogInterface dialog, int which) {
                                                                                    openUserMenu();
                                                                                }
                                                                            });
                                                                            Log.d(LOG_TAG, flexinAPI.getRequest().getResponse());
                                                                        } catch (Exception e) {
                                                                            e.printStackTrace();
                                                                        }
                                                                    }
                                                                });
                                                            } catch (final Exception e) {
                                                                Log.e(LOG_TAG, "error: " + e.getMessage());

                                                                runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        dialog.errorDialog(ReaderActivity.this, getString(R.string.data_collection), e.getMessage());
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    }).start();
                                                }
                                            });

                                    dialog.createDialog(alertDialogBuilder);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (final Exception e) {
                    Log.e(LOG_TAG, "error: " + e.getMessage());

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dialog.errorDialog(ReaderActivity.this, getString(R.string.data_collection), e.getMessage());
                        }
                    });
                }
            }
        }).start();
    }

    protected void createNewMaterial() {
        this.startActivity(new Intent(ReaderActivity.this, MaterialActivity.class));
    }
    protected void createNewMaterial(String type, String data) {
        Intent intent = new Intent(ReaderActivity.this, MaterialActivity.class);
        intent.putExtra(type, data);

        this.startActivity(intent);
    }

    protected void resumeReading() {
        scannerView.resumeCameraPreview(ReaderActivity.this);
    }

    protected void pauseReading() {
        scannerView.stopCameraPreview();
    }

    public void handleResult(final Result result) {
        if (result.getBarcodeFormat().toString().equals("QR_CODE")) {
            try {
                Log.d(LOG_TAG, result.getText());

                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ReaderActivity.this);
                alertDialogBuilder
                        .setTitle(R.string.qrcode_reading)
                        .setMessage(R.string.qrcode_not_recognized)
                        .setCancelable(false)
                        .setNegativeButton(R.string.ok, null)
                        .setNeutralButton(R.string.add, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        createNewMaterial("QRCode", result.getText());
                                    }
                                }
                        );

                dialog.createDialog(alertDialogBuilder);

            } catch (Exception e) {
                Log.e(LOG_TAG, e.getMessage());

                dialog.infoDialog(ReaderActivity.this, getString(R.string.qrcode_reading), e.getMessage(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
            }
        } else
            resumeReading();
    }
}

