package fr.utc.flexin.tools;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import java.util.List;

import fr.utc.flexin.R;
import fr.utc.flexin.ReaderActivity;
import fr.utc.flexin.adapters.OptionsAdapter;
import fr.utc.flexin.views.ZXingCustomScannerView;

/**
 * Created by Samy on 25/10/2017.
 */

public class Dialog {
    private static final String LOG_TAG = "_Dialog";

    private static Activity activity;
    private static AlertDialog alertDialog;
    private static AlertDialog.Builder alertDialogBuilder;
    private static ProgressDialog loading;

    protected Runnable onOpen;
    protected Runnable onClose;

    protected List<String> optionList;
    protected OptionsAdapter optionsAdapter;
    protected int optionSelected;

    public Dialog(final Activity activity) {
        this.activity = activity;
    }

    public void dismiss() {
        if (this.alertDialog != null)
            this.alertDialog.dismiss();

        if (this.loading != null)
            this.loading.dismiss();

        this.alertDialog = null;
        this.loading = null;
    }

    public Boolean isShowing() { return (this.alertDialog != null && this.alertDialog.isShowing()) || (this.loading != null && this.loading.isShowing()); }

    public void attachOnOpen(Runnable onOpen) {
        this.onOpen = onOpen;
    }

    public void attachOnClose(Runnable onClose) {
        this.onClose = onClose;
    }

    public void createMenu(final Activity activity, final String title, final List<String> optionList, final Runnable runnable) {
        final View menuView = activity.getLayoutInflater().inflate(R.layout.dialog_list, null);
        final ListView optionListView = menuView.findViewById(R.id.list_view);

        this.optionList = optionList;
        ArrayNode optionListAdded = (ArrayNode) new ObjectMapper().valueToTree(optionList);
        try {
            this.optionsAdapter = new OptionsAdapter(activity, optionListAdded);
        } catch (Exception e) {
            e.printStackTrace();
        }

        optionListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                optionSelected = position;

                runnable.run();
            }
        });
        optionListView.setAdapter(this.optionsAdapter);

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
        alertDialogBuilder
                .setTitle(title)
                .setView(menuView)
                .setCancelable(true);

        createDialog(alertDialogBuilder);
    }

    public int getOptionSelected() { return optionSelected; }

    public void createDialog() { createDialog((EditText) null); }
    public void createDialog(AlertDialog.Builder alertDialogBuilder) { createDialog(alertDialogBuilder, null); }
    public void createDialog(AlertDialog.Builder alertDialogBuilder, final EditText input) { this.alertDialogBuilder = alertDialogBuilder; createDialog(input); }
    public void createDialog(final EditText input) {
        dismiss();

        this.alertDialog = alertDialogBuilder.create();

        if (this.onClose != null) {
            this.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    onClose.run();
                }
            });
        }

        this.alertDialog.show();

        // Auto open keyboard
        if (input != null) {
            new Thread() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(250);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            input.requestFocus();
                            input.setFocusableInTouchMode(true);

                            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.showSoftInput(input, InputMethodManager.SHOW_IMPLICIT);
                        }
                    });
                }
            }.start();
        }

        new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(250);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (onOpen != null)
                            onOpen.run();
                    }
                });
            }
        }.start();
    }

    public void choiceDialog(final Activity activity, final String title, final String message, final DialogInterface.OnClickListener onClickListenerYes, final DialogInterface.OnClickListener onClickListenerNo) {
        this.activity = activity;
        dismiss();

        this.alertDialogBuilder = new AlertDialog.Builder(activity);
        this.alertDialogBuilder
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, onClickListenerYes)
                .setNegativeButton(R.string.no, onClickListenerNo);

        createDialog();
    }

    public void infoDialog(final Activity activity, final String title, final String message) { errorDialog(activity, title, message, null); }
    public void infoDialog(final Activity activity, final String title, final String message, final DialogInterface.OnClickListener onClickListener) {
        this.activity = activity;
        dismiss();

        this.alertDialogBuilder = new AlertDialog.Builder(activity);
        this.alertDialogBuilder
            .setTitle(title)
            .setMessage(message)
            .setCancelable(false)
            .setNegativeButton(R.string.ok, onClickListener);

        createDialog();
    }

    public void errorDialog(final Activity activity, final String title, final String message) { errorDialog(activity, title, message, null); }
    public void errorDialog(final Activity activity, final String title, final String message, final DialogInterface.OnClickListener onClickListener) {
        infoDialog(activity, title, message, onClickListener);
    }

    public void fatalDialog(final Activity activity, final String title, final String message) { errorDialog(activity, title, message, null); }
    public void fatalDialog(final Activity activity, final String title, final String message, final DialogInterface.OnClickListener onClickListener) {
        infoDialog(activity, title, message, onClickListener);
    }

    public void startLoading(Activity activity, final String title, final String message) {
        dismiss();
        this.loading = ProgressDialog.show(activity, title, message, true, false);
    }

    public void changeLoading(final String message) {
        if (this.loading != null)
            this.loading.setMessage(message);
    }

    public void stopLoading() {
        if (this.loading != null)
          this.loading.dismiss();

        this.loading = null;
    }
}
