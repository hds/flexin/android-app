package fr.utc.flexin.tools;

import android.app.Activity;
import android.util.Log;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.HashMap;
import java.util.Map;

import fr.utc.flexin.R;

/**
 * Created by Samy on 14/06/2018.
 */
public class FlexinAPI {
    private static final String LOG_TAG = "_FlexinAPI";
    private static final String url = "https://utc.nastuzzi.fr/flexin/public/api/";
    private String username;

    private HTTPRequest request;

    private String notLogged;
    private String serviceText;
    private String notFound;
    private String badRequest;
    private String internalError;
    private String forbiddenRequest;
    private String errorRequest;

    private Map<String, String> cookies = new HashMap<String, String>();

    public FlexinAPI(final Activity activity) {
        this.username = "";

        this.notLogged = activity.getString(R.string.no_longer_connected);
        this.serviceText = activity.getString(R.string.service);
        this.notFound = activity.getString(R.string.not_found);
        this.badRequest = activity.getString(R.string.bad_request);
        this.internalError = activity.getString(R.string.internal_error);
        this.forbiddenRequest = activity.getString(R.string.forbidden_request);
        this.errorRequest = activity.getString(R.string.error_request);
    }

    public Boolean isConnected() { return !this.username.isEmpty(); }

    public void disconnect() {
        this.username = "";

        if (!isConnected()) {
            try {
                logout();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getUsername() { return username; }
    public HTTPRequest getRequest() { return this.request; }

    public int getUserList() throws Exception {
        int reponseCode = request(
                "v1",
                "users"
        );

        return reponseCode;
    }

    public int getUser(final Integer id) throws Exception {
        int reponseCode = request(
                "v1",
                "users/" + id
        );

        return reponseCode;
    }

    public int addUser(final String lastname, final String firstname, final String email, final String phone, final String company, final String type) throws Exception {
        int reponseCode = request(
                "v1",
                "users",
                new HashMap<String, Object>() {{
                    put("lastname", lastname);
                    put("firstname", firstname);
                    put("email", email);
                    put("phone", phone);
                    put("company", company);
                    put("type", type);
                }}
        );

        return reponseCode;
    }

    public int editUser(final int id, final String lastname, final String firstname, final String email, final String phone, final String company, final String type) throws Exception {
        int reponseCode = request(
                "v1",
                "users/" + id,
                new HashMap<String, Object>() {{
                    put("lastname", lastname);
                    put("firstname", firstname);
                    put("email", email);
                    put("phone", phone);
                    put("company", company);
                    put("type", type);
                }},
                "PUT"
        );

        return reponseCode;
    }

    public int delUser(final int id) throws Exception {
        int reponseCode = request(
                "v1",
                "users/" + id,
                new HashMap<String, Object>(),
                "DELETE"
        );

        return reponseCode;
    }

    public int getInfrastructureList() throws Exception {
        int reponseCode = request(
                "v1",
                "infrastructures"
        );

        return reponseCode;
    }

    public int getInfrastructure(final Integer id) throws Exception {
        int reponseCode = request(
                "v1",
                "infrastructures/" + id
        );

        return reponseCode;
    }

    public int addInfrastructure(final String name, final String description) throws Exception {
        int reponseCode = request(
                "v1",
                "infrastructures",
                new HashMap<String, Object>() {{
                    put("name", name);
                    put("description", description);
                }}
        );

        return reponseCode;
    }

    public int editInfrastructure(final int id, final String name, final String description) throws Exception {
        int reponseCode = request(
                "v1",
                "infrastructures/" + id,
                new HashMap<String, Object>() {{
                    put("name", name);
                    put("description", description);
                }},
                "PUT"
        );

        return reponseCode;
    }

    public int delInfrastructure(final int id) throws Exception {
        int reponseCode = request(
                "v1",
                "infrastructures/" + id,
                new HashMap<String, Object>(),
                "DELETE"
        );

        return reponseCode;
    }

    public int getCategoryList() throws Exception {
        int reponseCode = request(
                "v1",
                "categories"
        );

        return reponseCode;
    }

    public int getCategory(final Integer id) throws Exception {
        int reponseCode = request(
                "v1",
                "categories/" + id
        );

        return reponseCode;
    }

    public int addCategory(final String name, final Integer parentId) throws Exception {
        int reponseCode = request(
                "v1",
                "categories",
                new HashMap<String, Object>() {{
                    put("name", name);

                    if (parentId != null)
                        put("parent_id", parentId);
                }}
        );

        return reponseCode;
    }

    public int editCategory(final int id, final String name, final Integer parentId) throws Exception {
        int reponseCode = request(
                "v1",
                "categories/" + id,
                new HashMap<String, Object>() {{
                    put("name", name);
                    put("parent_id", parentId);
                }},
                "PUT"
        );

        return reponseCode;
    }

    public int delCategory(final int id) throws Exception {
        int reponseCode = request(
                "v1",
                "categories/" + id,
                new HashMap<String, Object>(),
                "DELETE"
        );

        return reponseCode;
    }

    public int loginBadge(final String badgeId) throws Exception {
        int reponseCode = request(
            "login",
            badgeId
        );

        JsonNode response;
        if (reponseCode == 200 && this.request.isJSONResponse())
            response = this.request.getJSONResponse();
        else
            throw new Exception(this.notLogged);

        if (response.has("name"))
            this.username = response.get("name").textValue();
        else
            throw new Exception("Unexpected JSON");

        return reponseCode;
    }

    public int logout() throws Exception {
        if (!isConnected())
            throw new Exception(this.notLogged);

        return request(
            null,
            "logout"
        );
    }

    protected int request(final String version, final String service) throws Exception { return request(version, service, new HashMap<String, Object>(), "GET"); }
    protected int request(final String version, final String service, final Map<String, Object> args) throws Exception { return request(version, service, args, "POST"); }
    protected int request(final String version, final String service, final Map<String, Object> args, final String verb) throws Exception {
        this.request = new HTTPRequest(url + (version == null ? "" : (version + "/")) + service);
        this.request.setCookies(this.cookies);
        int responseCode;

        if (verb.equals("GET")) {
            Map<String, String> getArgs = new HashMap<String, String>();

            for (String name : args.keySet())
                getArgs.put(name, args.get(name).toString());

            this.request.setGet(getArgs);
            responseCode = this.request.get();
        }
        else {
            Log.d(LOG_TAG, args.toString());
            this.request.setPost(args);
            responseCode = this.request.post(true, verb);
        }

        this.cookies = request.getCookies();

        if (responseCode >= 200 || responseCode < 300)
            return responseCode;
        else if (responseCode == 403) {
            if (this.request.isJSONResponse()) {
                if (this.request.getJSONResponse().get("error").get("message").textValue().contains("must be logged"))
                    throw new Exception(this.notLogged);
            }

            throw new Exception(this.serviceText + " " + service + " " + this.forbiddenRequest);
        }
        else if (responseCode == 404)
            throw new Exception(this.serviceText + " " + service + " " + this.notFound);
        else if (responseCode == 400) {
            if (this.request.isJSONResponse()) {
                if (this.request.getJSONResponse().has("error") && this.request.getJSONResponse().get("error").has("message"))
                    throw new Exception(this.request.getJSONResponse().get("error").get("message").textValue());
            }

            throw new Exception(this.serviceText + " " + service + " " + this.badRequest);
        }
        else if (responseCode == 500 || responseCode == 503) {
            throw new Exception(this.serviceText + " " + service + " " + this.internalError);
        }
        else
            throw new Exception(this.serviceText + " " + service + " " + this.errorRequest + " " + responseCode);
    }
}
