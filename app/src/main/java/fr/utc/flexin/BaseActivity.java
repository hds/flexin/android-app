package fr.utc.flexin;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;

import fr.utc.flexin.tools.Config;
import fr.utc.flexin.tools.Dialog;
import fr.utc.flexin.tools.FlexinAPI;

/**
 * Created by Samy on 26/10/2017.
 */

public abstract class BaseActivity extends InternetActivity {
    private static final String LOG_TAG = "_BaseActivity";

    protected static FlexinAPI flexinAPI;

    protected static Config config;
    protected static Dialog dialog;

    protected static SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= 24) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
        }

        this.flexinAPI = new FlexinAPI(this);
        this.dialog = new Dialog(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        dialog.dismiss();
    }

    protected void disconnect() {
        flexinAPI.disconnect();
    }

    protected void unregister(final Activity activity) {
        disconnect();

        dialog.errorDialog(activity, getString(R.string.key_registration), getString(R.string.key_remove_temp));
    }

    protected void fatal(final Activity activity, final String title, final String message) {
        dialog.fatalDialog(activity, title, message, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                unregister(activity);
                restartApp(activity);
            }
        });
    }

    protected void restartApp(final Activity activity) {
        startMainActivity(activity);
    }

    protected void startMainActivity(final Activity activity) {
        disconnect();

        Intent intent = new Intent(activity, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        finish();
        activity.startActivity(intent);
    }

    protected void startAPIActivity(final Activity activity) {
        if (haveCameraPermission())
            startActivity(new Intent(activity, ReaderActivity.class));
        else
            dialog.errorDialog(BaseActivity.this, getString(R.string.qrcode), getString(R.string.need_camera_permission));
    }

    protected boolean haveStoragePermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else
            return true;
    }

    protected boolean haveCameraPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 1);
                return false;
            }
        }
        else
            return true;
    }
}
